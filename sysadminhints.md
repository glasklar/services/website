# For admins of web services at Glasklar

Sites run at listen.sigsum.org.  The below `build.sh` script essentially does
`git-pull` and `rsync` to track the branches of the respective websites. 

**Note:** there's currently no automation.  Run the below manually.

## Deploying blog.system-transparency.org

    $ (cd /home/cper && sudo -u cper ./build.sh blog-sto)

**Warning:** it appears that we need to do some clean-up in the blog template if
we want to continue using it, (newer) `hugo` doesn't run out of the box anymore.

## Deploying www.glasklarteknik.se

    $ (cd /home/cper && sudo -u cper ./build.sh www-gtse)

## Deploying www.sigsum.org

    $ (cd /home/cper && sudo -u cper ./build.sh www-so)

## Deploying www.system-transparency.org

    $ (cd /home/cper && sudo -u cper ./build.sh www-sto)

## docs.system-transparency.org 

Unpack release tar file in /home/cper/system-transparency-docs.  If the version
is st-1.1.0, invoke build.sh as follows:

    $ (cd /home/cper && sudo -u cper ./build.sh docs-sto 1.1.0)

## Installing a new certificate

When adding a new name to `certbot_certs.domains[]` a new cert
including the new name will not be generated automatically despite
`certbot_create_if_missing` = true.

It can be fixed manually by enumerating all the domain names and
invoking certbot with `--expand` in a shell:

    export DOMAINLIST=name1,name2,...
	certbot certonly -d $DOMAINLIST --pre-hook "systemctl stop apache2" --post-hook "systemctl start apache2" --standalone --expand

##  Renewing onion certificates with HARICA

In the general case, there are two CSR's to submit. One for the X.509
cert and one for the onion key verification. The onion key were not
verified when renewing the previous two times (2022, 2023). In 2024 we
tried to go the Onion CSR route but failed, see below

- Log in to HARICA's CertManager at https://cm.harica.gr/
- Select the onion cert (er3n3jnvoy...) from the list
- Choose the action "Request again" for the "www.sigsum.org" cert in the Expiring Certificates list
- Click Next at the Certificate details screen
- Select 1 year and click Next
- Click Submit CSR manually
- On listen.sigsum.org, run the following commands as root
  cd /etc/apache2/certs
  mkdir $(date +%Y); cd $(date +%Y)
  openssl req -new -key ../er3n3jnvoyj2t37yngvzr35b6f4ch5mgzl3i6qlkvyhzmaxo62nlqmqd.onion.key.pem -out csr.pem
    <enter a dot (.) on all except Common Name>
    Common Name (e.g. server FQDN or YOUR name) []:er3n3jnvoyj2t37yngvzr35b6f4ch5mgzl3i6qlkvyhzmaxo62nlqmqd.onion
  openssl req -noout -text < csr.pem
  =>
    Certificate Request:
	...
            Subject: CN = er3n3jnvoyj2t37yngvzr35b6f4ch5mgzl3i6qlkvyhzmaxo62nlqmqd.onion
- Insert the output from cat csr.pem in the web browser, check the "I,
  N.N., declare that ..." box and click Submit request
- Confirm that you really want to use the same key again
- Wait for an email with a string to publish under
  http://er3n3jnvoyj2t37yngvzr35b6f4ch5mgzl3i6qlkvyhzmaxo62nlqmqd.onion/.well-known/pki-validation/
- Note that Documentroot for er3n3...onion is /var/www/www and create a file with the content from the email
- Select "Continue to Payment", enter payment info, request invoice and enter VAT number
- If you're in TB, the MasterCard ID Check window will be blank but **don't do anything** except starting BankID and follow procedure
- In "Get your certificate", select PEM, download the cert and copy it to listen.so:/etc/apache2/certs/$(date +%Y)/
- Symlink the new cert file to /etc/apache2/certs/er3n3jnvoyj2t37yngvzr35b6f4ch5mgzl3i6qlkvyhzmaxo62nlqmqd.onion.cert.pem
- Reload apache (systemctl reload apache2)


### The (failing) Onion CSR route

- Back at the dashboard and Pending Certificates, select "Change Domain validation" select "Submit Onion CSR"
- On listen.sigsum.org, run the following commands as a non root user, replacing $NONCE with the "CA signing nonce"
  - FIXME: fix file perms for !root to read /var/lib/tor/hs_www/hs_ed25519_secret_key
  go install sauteed-onions.org/onion-csr@latest
  ${GOBIN-~/go/bin}/onion-csr -n $NONCE -d /var/lib/tor/hs_www
- In "Submit Onion CSR", paste the CSR from the output of onion-csr and select "Submit"
  - Note: Submit button is seeming not doing anything in TB (URL https://cm.harica.gr/ServerCertificates/Validation_0)
  - Tried TB  security levels Safer and Standard. I did see an HTTP request
    ("GET / HTTP/1.1" => 200 4644) with agent string "GuzzleHttp/7"
    once after pressing submit (security level Standard). Same
    results in plain FF (Fedora 130.0.1 and macOS 129.0.2).
  - Fun detail: Following the link in email "HARICA - Submit Onion CSR Validation" took me to a "Login loop".
  - I've tried to both include and not include the PEM header and footer (BEGIN/END CERTIFICATE REQUEST)
