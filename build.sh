#! /bin/bash

set -eu

# This script deploys some web sites on listen.sigsum.org.
# Should be run as user cper. It is assumed that the git
# repository of each website has already been initialized.

CPER_UID=1005

usage() {
	echo "usage: build.sh {docs-sto VERSION|www-sto|blog-sto|www-so|www-gtse}"
	exit 1
}

# Assumes we have already unpacked the release tar file
docs_sto() {
	local dir=$1; shift
	local dest=$1; shift

	"$dir"/assemble.sh
	_hugo "$dir" "$dest"
}

www_sto() {
	local dir="$1"; shift
	local dest="$1"; shift

	git -C "$dir" pull
	hugo -s "$dir"
	rsync -av --delete --exclude "/keys" "$dir/public/" "$dest/"
}

www_gtse() {
	local dir="$1"; shift
	local dest="$1"; shift

	git -C "$dir" pull
	hugo -s "$dir"
	rsync -av --delete --exclude "/archive" "$dir/public/" "$dest/"
}

# TODO: pull_and_hugo_with_exclude (or similar). So we don't need to have
# separate www_sto and www_gtse.
pull_and_hugo() {
	local dir="$1"; shift
	local dest="$1"; shift

	git -C "$dir" pull
	_hugo "$dir" "$dest"
}


_hugo() {
	local dir="$1"; shift
	local dest="$1"; shift
	hugo -s "$dir"
	rsync -av --delete "${dir}/public/" "${dest}/"
}

[[ $(id -u) != "$CPER_UID" ]] && { echo "$0: user $(id -u) != $CPER_UID"; exit 1; }
[[ $# == 0 ]] && usage
case "$1" in
	www-gtse)
		www_gtse ~/glasklar-www/www.glasklarteknik.se /var/www/www.glasklarteknik.se
		;;
	www-so)
		pull_and_hugo ~/sigsum-www/www.sigsum.org /var/www/www
		;;
	www-sto)
		www_sto ~/system-transparency-www /var/www/www.system-transparency.org
		;;
	blog-sto)
	    	git -C ~/system-transparency-blog pull
		_hugo ~/system-transparency-blog pull /var/www/blog.system-transparency.org
		;;
	docs-sto)
		docs_sto ~/system-transparency-docs/st-"$2"/docs /var/www/docs.system-transparency.org/st-"$2"
		;;
	*)
		usage
		;;
esac
