Welcome to Glasklar Teknik AB!

We do open source work relating to the following projects:

  - Sigsum: <https://www.sigsum.org/>
  - System Transparency: <https://www.system-transparency.org/>

We also help operating internet infrastructure:

  - A Sigsum transparency log: [seasalp.glasklar.is](https://www.sigsum.org/services/)
  - Bastion host for transparency log witnesses: `bastion.glasklar.is`
  - Debian's snapshot service: <https://snapshot.debian.org/>
