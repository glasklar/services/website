# About

Glasklar Teknik AB is a sister company of [Mullvad VPN AB][] and [Tillitis
AB][].  The team is composed of:

  - Fredrik Strömberg (kfreds)
  - Linus Nordberg (ln5)
  - Niels Möller (nisse)
  - Rasmus Dahlberg (rgdd)
  - Elias Rudberg (elias)

Our headquarters is located in Stockholm, Sweden.

[Mullvad VPN AB]: https://www.mullvad.net/
[Tillitis AB]: https://www.tillitis.se/
